# encoding: utf8
# @author reijaff
# @category Examples

import argparse

def run_script(server_host, server_port):
    import ghidra_bridge
    import syms2elf_HELPER
    import os


    SHN_UNDEF = 0
    STB_GLOBAL_FUNC = 0x12

    class Symbol:
        def __init__(self, name, info, value, size, shname, shndx=-1):
            self.name   = name
            self.info   = int(info)
            self.value  = int(value)
            self.size   = int(size)
            self.shname = shname
            self.shndx  = shndx
        def __repr__(self):
            return "%s\t%s\t%s\t%s\t%s" % (self.name, self.info, self.value, self.size, self.shname)

    def get_original_image_base():
        props = currentProgram.getOptions(currentProgram.PROGRAM_INFO)
        oibStr = props.getString(ghidra.app.util.opinion.ElfLoader.ELF_ORIGINAL_IMAGE_BASE_PROPERTY, None);
        if oibStr is not None:
            return ghidra.util.NumericUtilities.parseHexLong(oibStr)
                
    def get_ghidra_section(addr):
        sections = currentProgram.getMemory().getBlocks()
        for idx, s in enumerate(sections):
            try:
                startAddr = int(str(s.getStart()), 16)
                endAddr = int(str(s.getEnd()), 16)
                if (startAddr <= addr <= endAddr) == True:
                    return (idx, s.getName())
            except:
                pass
        return None

    def ghidra_fnc_filter(fnc):
        if fnc.isThunk() == True or fnc.isExternal() == True:
            return False
        return True

    def get_ghidra_symbols():
        symbols = []
        functions = currentProgram.getFunctionManager().getFunctions(True)
        for fnc in filter(ghidra_fnc_filter, functions):
            if 'syscall' in str(fnc.getEntryPoint()):
                continue
            fnc_addr = int(str(fnc.getEntryPoint()), 16)
            fnc_name = fnc.getName()
            fnc_size = fnc.getBody().getNumAddresses()
            sh_idx, sh_name = get_ghidra_section(fnc_addr)
            symbols.append(Symbol(fnc.name, STB_GLOBAL_FUNC,
                get_original_address(fnc_addr), fnc_size, sh_name))
        return symbols

    def get_original_address(addr):
        current_base_addr = ghidra.util.NumericUtilities.parseHexLong(currentProgram.minAddress.toString()) # meh
        original_base_addr = get_original_image_base()
        offset = original_base_addr - current_base_addr
        return addr + offset


    # create the bridge and load the flat API/ghidra modules into the namespace
    with ghidra_bridge.GhidraBridge(connect_to_host=server_host, connect_to_port=server_port, namespace=globals()):
        
        infile = currentProgram.getDomainFile().getMetadata()["Executable Location"][1:]
        outfile = infile + '.sym'

        symbols = get_ghidra_symbols()

        syms2elf_HELPER.write_symbols(infile,outfile,symbols)
        

if __name__ == "__main__":

    in_ghidra = False
    try:
        import ghidra
        in_ghidra = True
    except ModuleNotFoundError:
        pass

    if in_ghidra:
        import ghidra_bridge_server
        script_file = getSourceFile().getAbsolutePath()
        ghidra_bridge_server.GhidraBridgeServer.run_script_across_ghidra_bridge(script_file)
    else:
        parser = argparse.ArgumentParser(
            description="Example py3 script that's expected to be called from ghidra with a bridge")
        parser.add_argument("--connect_to_host", type=str, required=False,
                            default="127.0.0.1", help="IP to connect to the ghidra_bridge server")
        parser.add_argument("--connect_to_port", type=int, required=True,
                            help="Port to connect to the ghidra_bridge server")

        args = parser.parse_args()

        run_script(server_host=args.connect_to_host,
                   server_port=args.connect_to_port)
        
